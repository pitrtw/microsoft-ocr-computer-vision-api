# Microsoft OCR #

此專案主要是用[Computer Vision API](https://www.microsoft.com/cognitive-services/en-us/computer-vision-api)來分析圖片上的文字

### 編譯前 ###

* 先把"yourSubscriptionKey"那行，改成你的Subscription Key，這樣才會成功!

```
#!c#

const string yourSubscriptionKey = "yourSubscriptionKey";
```