﻿using Microsoft.ProjectOxford.Vision;
using Microsoft.ProjectOxford.Vision.Contract;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace OCR
{
    public partial class Main : Form
    {
        private OpenFileDialog _openFileDialog = new OpenFileDialog();

        private VisionServiceClient _visionClient = null;

        public Main()
        {
            InitializeComponent();
            const string yourSubscriptionKey = "8cb79ea8069d417993938b3b069ffb0a";
            _visionClient = new VisionServiceClient(yourSubscriptionKey);
            _openFileDialog.Filter = "JPG (*.jpg, *.jpeg)|*.jpg;*.jpeg";
        }

        private async void btnSelectImage_Click(object sender, EventArgs e)
        {
            if (_openFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxResult.Text = "處理中...";
                var filePath = _openFileDialog.FileName;
                txtBoxFilePath.Text = filePath;
                pictureBox.Image = Image.FromFile(_openFileDialog.FileName);
                using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    try
                    {
                        var ocrResults = await _visionClient.RecognizeTextAsync(fileStream, LanguageCodes.AutoDetect);
                        textBoxResult.Text = string.Empty;
                        foreach (var region in ocrResults.Regions)
                        {
                            foreach (var line in region.Lines)
                            {
                                foreach (var word in line.Words)
                                {
                                    textBoxResult.Text = $"{textBoxResult.Text}{word.Text}";
                                }
                                textBoxResult.Text = $"{textBoxResult.Text}{Environment.NewLine}";
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        textBoxResult.Text = exception.StackTrace;
                    }
                }
            }
        }
    }
}